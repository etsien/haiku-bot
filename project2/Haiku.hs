import System.IO
import Data.List
import Data.List.Split
import Data.Char
import Data.Char(isDigit)

main = do
	contents <- readFile "Dictionary1.txt"
	putStrLn "enter text: "
	inputstr <- getLine
	print (haiku $ syllCount (words $ removePunct inputstr) (map splitter (lines contents)))


syllCount :: [String] -> [(String,String)] -> Integer
syllCount [] _ = 0
syllCount (word:words) wordlist = readInt (lookup1 word wordlist) + syllCount words wordlist

removePunct :: String -> String
removePunct xs = [toLower x | x<-xs, not (x `elem` ",.?!")]

haiku :: Integer -> String
haiku x
	| x == 17 = "This is a haiku!"
	| x < 17 = "This is not a haiku, too few syllables."
	| x > 17 || x > 0 = "This is not a haiku, too many syllables."
	| otherwise = "Unrecognized Word!"

splitter :: String -> (String, String)
splitter x = (a,c)
	where
	a = head (words x)
	--b = head (tail (words x))
	c = last (words x)

readInt :: String -> Integer
readInt = read

lookup1 :: String -> [(String,String)] -> String
lookup1 a [] = "0"
lookup1  key ((x,y):xys)
    | key == x          =  y
    | otherwise         =  lookup1 key xys

--first :: (a,b,c) -> a
--first (a,_,_) = a

--second :: (a,b,c) -> b
--second (_,b,_) = b

--third :: (a,b,c) -> c
--third (_,_,c) = c
