# README #

01/11/2016

Edward Tsien


Quick little haiku generator and validator.

* Haiku.hs asks for a poem, then returns if it is.
* HaikuGen.hs will create a haiku built from words provided in Dictionary1.txt
* Dictionary1.txt is the generated list combining the various SyllableLists.
* Syllable.hs is a script that generates Dictionary1.txt
* There are over 60,000 words at its disposal.